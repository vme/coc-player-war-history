package com.boot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.boot.utility.UtilityClass;

public class UtilityClassTest {

	@Test
	public void testRemoveHashInTag() {
		final String TAG1 = "#TEST_STRING";
		final String TAG2 = "#TEST_#STRING";

		assertEquals("TEST_STRING", UtilityClass.removeHashInTag(TAG1));
		assertEquals("TEST_STRING", UtilityClass.removeHashInTag(TAG2));
	}
}
