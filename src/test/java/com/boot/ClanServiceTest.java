package com.boot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.boot.model.ClanMembers;
import com.boot.model.Player;
import com.boot.repository.PlayerRepository;
import com.boot.service.ClanService;
import com.boot.testdata.TestData;
import com.boot.utility.UtilityClass;

public class ClanServiceTest {

	@InjectMocks
	private ClanService clanService;

	@InjectMocks
	private RestTemplate restTemplate;

	@Mock
	private PlayerRepository playerRepository;

	@Test
	public void testaddClan() {
		when(restTemplate.exchange("https://api.clashofclans.com/v1/clans/%23Y0JC80LR8", HttpMethod.GET,
				UtilityClass.generateCOCHeaderEntity(), ClanMembers.class).getBody())
						.thenReturn(TestData.getClanMembers());

		List<Player> playersAdded = clanService.addClan("Y0JC80LR8");
		assertEquals(TestData.getPlayerList(), playersAdded);
	}

	@Test
	public void testFindAllClans() {
		Set<String> clans = TestData.getClanList();
		when(playerRepository.findAllClans()).thenReturn(clans);
		assertSame(clans, clanService.findAllClans());
	}
}
