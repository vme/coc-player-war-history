package com.boot.testdata;

import java.util.List;
import java.util.Set;

import com.boot.model.ClanMembers;
import com.boot.model.MemberTag;
import com.boot.model.PlayerInfo;

public final class TestData {

	public static PlayerInfo getPlayerInfo() {
		return null;
	}

	public static List<String> getPlayerList() {
		return null;
	}

	public static Set<String> getClanList() {
		return null;
	}

	public static ClanMembers getClanMembers() {
		ClanMembers clan = new ClanMembers();
		List<MemberTag> members = clan.getItems();
		for (int i = 0; i < 10; ++i) {
			MemberTag tag = new MemberTag();
			tag.setTag("Y0JC80LR" + i);
			members.add(tag);
		}
		return clan;

	}

}
