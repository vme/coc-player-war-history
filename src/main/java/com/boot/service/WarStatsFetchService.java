package com.boot.service;

import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.boot.model.Attack;
import com.boot.model.ClanWarState;
import com.boot.model.Member;
import com.boot.model.WarStat;
import com.boot.repository.PlayerRepository;
import com.boot.repository.WarStatsRepository;
import com.boot.utility.UtilityClass;

@Service
public class WarStatsFetchService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String URL_PART1 = "https://api.clashofclans.com/v1/clans/%23";
	private static final String URL_PART2 = "/currentwar";
	private Map<String, Timestamp> ignoredClansMap = new ConcurrentHashMap<>();
	private RestTemplate restTemplate;
	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private WarStatsRepository warStatsRepository;

	public WarStatsFetchService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Scheduled(cron = "*/5 * * * * *")
	public void cronJob() {
		logger.info("> cron task start");
		Set<String> clans = playerRepository.findAllClans();

		// Check if the ignore time for clans are still valid in the ignoredClansMap,
		// nothing will be returned
		validateIgnoredClansList();
		clans = filterPersistanceList(clans);

		List<ClanWarState> clansToStore = fetchValidClansForAPICall(clans);

		for (ClanWarState clanWar : clansToStore) {
			if (clanWar != null && clanWar.getClan() != null) {
				String clanTag = clanWar.getClan().getTag();
				clanTag = UtilityClass.removeHashInTag(clanTag);
				List<WarStat> warStatsList = getWarStats(clanTag, clanWar.getEndTime());
				if (warStatsList.size() != 0) {
					ignoredClansMap.put(UtilityClass.removeHashInTag(clanTag),
							UtilityClass.getCurrentDateTimePlus48hrs());
				} else {
					Timestamp warEndTime = clanWar.getEndTime();
					List<Member> membersStats = clanWar.getClan().getMembers();
					for (Member membersStat : membersStats) {
						String playerId = UtilityClass.removeHashInTag(membersStat.getTag());
						// There can only be a max of two attacks per member each war
						persistMemberStatAttacks(membersStat, playerId, warEndTime, clanTag);
					}
				}
			}
		}

		logger.info(clans.toString());
		logger.info("< cron task end");
	}

	/**
	 * Method to persist member war stat to database
	 * 
	 * @param membersStat
	 * @param playerId
	 * @param warEndTime
	 * @param clanTag
	 */
	private void persistMemberStatAttacks(Member membersStat, String playerId, Timestamp warEndTime, String clanTag) {
		// There can only be a max of two attacks per member
		if (membersStat.getAttacks() != null) {
			for (Attack attack : membersStat.getAttacks()) {
				WarStat warStat = new WarStat();
				warStat.setPlayerID(playerId);
				warStat.setDefenderTag(UtilityClass.removeHashInTag(attack.getDefenderTag()));
				warStat.setStars(attack.getStars());
				warStat.setDestructionPercentage(attack.getDestructionPercentage());
				warStat.setWarEndTime(warEndTime);
				warStatsRepository.saveAndFlush(warStat);
				ignoredClansMap.put(UtilityClass.removeHashInTag(clanTag), UtilityClass.getCurrentDateTimePlus48hrs());
			}
		}
	}

	/**
	 * Method to make an API call to COC to get individual clan war datails
	 * 
	 * @param clans
	 * @return list of ClanWarState
	 */
	private List<ClanWarState> fetchValidClansForAPICall(Set<String> clans) {
		List<ClanWarState> clansToStore = new ArrayList<>();
		for (String clan : clans) {
			// TODO: handle 403 exceptions
			ClanWarState warState = getClanWarDetails(clan);
			// Check if clan war state is "warEnded", if yes add to persistence list
			if (warState != null && ClanWarsStates.WarEnded.equalsState(warState.getState())) {
				clansToStore.add(warState);
			}
		}
		return clansToStore;
	}

	/**
	 * Method to get clan war details from COC API
	 * 
	 * @param clan
	 * @return ClanWarState
	 */
	private ClanWarState getClanWarDetails(String clan) {
		logger.info("> Making an COC current war api call");
		ClanWarState clanWarState = null;
		clan = UtilityClass.removeHashInTag(clan);
		try {
			URI uri = new URI(URL_PART1 + clan + URL_PART2);
			logger.info(uri.toASCIIString());
			clanWarState = restTemplate
					.exchange(uri, HttpMethod.GET, UtilityClass.generateCOCHeaderEntity(), ClanWarState.class)
					.getBody();
			logger.info("< COC current war api call completed");
		} catch (RestClientException ex) {
			logger.info("restful request for COC current war api failed with exception");
			logger.error(ex.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return clanWarState;
	}

	/**
	 * Method to filer the clans list based on the Ignore clan hashmap
	 * 
	 * @param clansToStore
	 * @return
	 */
	private Set<String> filterPersistanceList(Set<String> clansToStore) {
		Set<String> clansToBeIgnored = ignoredClansMap.keySet();
		clansToStore.removeAll(clansToBeIgnored);
		return clansToStore;
	}

	/**
	 * Method to get war stats from database
	 * 
	 * @param clanTag
	 * @param warEndTime
	 * @return
	 */
	private List<WarStat> getWarStats(String clanTag, Timestamp warEndTime) {
		return warStatsRepository.findByClanIDandWarEndTime(clanTag, warEndTime.toString());
	}

	/**
	 * Method to validate the clans in HashMap based on current Timestamp If the
	 * value for the clan Timestamp is lesser than the current time remove it from
	 * the HashMap
	 */
	private void validateIgnoredClansList() {
		Set<Entry<String, Timestamp>> clanEntrySet = ignoredClansMap.entrySet();
		for (Entry<String, Timestamp> clanEntry : clanEntrySet) {
			if (UtilityClass.getCurrentDateTime().compareTo(clanEntry.getValue()) > 0) {
				ignoredClansMap.remove(clanEntry.getKey());
			}
		}
	}
}
