package com.boot.service;

public enum ClanWarsStates {

	/*
	 * Four states for clans to be in
	 * 
	 * 1) Clan state changes to "preparation" once the clan starts preparation for
	 * the war (24hrs). 2) Clan state changes to "inWar" once the clan has completed
	 * the preparation time for the war (24hrs). 3) Clan state changes to "warEnded" once
	 * the war time has ended (This is when we need to obtain the war stats). 4)
	 * Clan state changes to "notInWar" once the clan initiates a search for another
	 * war
	 */
	WarEnded("warEnded"), Preparation("preparation"), InWar("inWar"), NotInWar("notInWar");

	private final String state;

	private ClanWarsStates(String state) {
		this.state = state;
	}

	public boolean equalsState(String otherState) {
		return state.equals(otherState);
	}

	@Override
	public String toString() {
		return this.state;
	}
}
