package com.boot.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.boot.model.ClanMembers;
import com.boot.model.MemberTag;
import com.boot.model.Player;
import com.boot.repository.PlayerRepository;
import com.boot.utility.UtilityClass;

@Service
public class ClanService {

	private static final String URL_PART1 = "https://api.clashofclans.com/v1/clans/%23";
	private static final String URL_PART2 = "/members";

	@Autowired
	private PlayerRepository playerRepository;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private RestTemplate restTemplate;

	public ClanService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	/**
	 * Returns distinct list of clans from the database
	 * 
	 * @return Set<String> clan set
	 */
	public Set<String> findAllClans() {
		return playerRepository.findAllClans();
	}

	/**
	 * Adds clan members to the database and return the added player list
	 * 
	 * @param clanTag
	 * @return List<Player> player list
	 */
	public List<Player> addClan(String clanTag) {
		clanTag = UtilityClass.removeHashInTag(clanTag);
		ClanMembers members = getClanMembers(clanTag);
		List<Player> players = persistMemberEntity(members, clanTag);
		return players;
	}

	/**
	 * Method to save the list of clan members to database
	 * @param members
	 * @param clanTag
	 * @return player list
	 */
	private List<Player> persistMemberEntity(ClanMembers members, String clanTag) {
		List<MemberTag> clanMembers = members.getItems();
		List<Player> players = new ArrayList<Player>();
		if (clanMembers != null) {
			for (MemberTag member : clanMembers) {
				Player player = new Player(UtilityClass.removeHashInTag(member.getTag()), clanTag);
				players.add(player);
				playerRepository.saveAndFlush(player);
			}
		}
		return players;
	}

	/**
	 * Method to return the member list for the clanTag
	 * makes an API call to the COC API
	 * @param clanTag
	 * @return
	 */
	private ClanMembers getClanMembers(String clanTag) {
		ClanMembers members = null;
		logger.info("> Making an COC clan members api call");
		try {
			URI uri = new URI(URL_PART1 + clanTag + URL_PART2);
			logger.info(uri.toASCIIString());
			members = restTemplate
					.exchange(uri, HttpMethod.GET, UtilityClass.generateCOCHeaderEntity(), ClanMembers.class).getBody();
			logger.info("> COC clan members api call completed");
		} catch (RestClientException ex) {
			logger.info("restful request for COC clan members api failed with exception");
			logger.error(ex.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return members;
	}

}
