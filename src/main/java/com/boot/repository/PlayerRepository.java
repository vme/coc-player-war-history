package com.boot.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boot.model.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {

	final static String FIND_ALL_PLAYERS_QUERY = "SELECT PLAYERID FROM PLAYER";
	final static String FIND_DISTINCT_CLANS_QUERY = "SELECT DISTINCT(CLANID)FROM PLAYER";

	List<Player> findByPlayerID(String playerID);

	@Query(value = FIND_ALL_PLAYERS_QUERY, nativeQuery = true)
	List<String> findAllPlayers();

	@Query(value = FIND_DISTINCT_CLANS_QUERY, nativeQuery = true)
	Set<String> findAllClans();
}
