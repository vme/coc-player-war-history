package com.boot.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "warstat")
public class WarStat {

//	@Transient
//	private static int idIncrementValue = 1;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "playerid")
	private String playerID;

	@Column(name = "defender_tag")
	private String defenderTag;

	@Column(name = "stars")
	private int stars;

	@Column(name = "destruction_percentage")
	private int destructionPercentage;

	@Column(name = "war_end_time")
	private Timestamp warEndTime;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlayerID() {
		return playerID;
	}
	
	public String getDefenderTag() {
		return defenderTag;
	}

	public int getStars() {
		return stars;
	}
	
	public int getDestructionPercentage() {
		return destructionPercentage;
	}

	public Timestamp getWarEndTime() {
		return warEndTime;
	}

	public void setDefenderTag(String defenderTag) {
		this.defenderTag = defenderTag;
	}

	public void setDestructionPercentage(int destructionPercentage) {
		this.destructionPercentage = destructionPercentage;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public void setWarEndTime(Timestamp warEndTime) {
		this.warEndTime = warEndTime;
	}
}
