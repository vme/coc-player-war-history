package com.boot.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClanMembers {

	private List<MemberTag> items;

	public List<MemberTag> getItems() {
		return items;
	}

	public void setItems(List<MemberTag> items) {
		this.items = items;
	}
}
