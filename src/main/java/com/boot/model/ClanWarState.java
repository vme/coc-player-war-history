package com.boot.model;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClanWarState {
	private String state;
	private Timestamp endTime;
	private WarClan clan;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		final String DATE_FORMAT = "yyyyMMdd'T'HHmmss'.'SSS'Z'";
		SimpleDateFormat sd = new SimpleDateFormat(DATE_FORMAT);
		Long timestamp = null;
		try {
			timestamp = sd.parse(endTime).getTime();
		} catch (ParseException ex) {
			logger.error(ex.getMessage());
		}
		this.endTime = new Timestamp(timestamp);
	}

	public WarClan getClan() {
		return clan;
	}

	public void setClan(WarClan clan) {
		this.clan = clan;
	}

}
