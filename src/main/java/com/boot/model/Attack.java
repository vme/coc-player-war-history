package com.boot.model;

public class Attack {
	private String defenderTag;
	private int stars;
	private int destructionPercentage;

	public String getDefenderTag() {
		return defenderTag;
	}

	public void setDefenderTag(String defenderTag) {
		this.defenderTag = defenderTag;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public int getDestructionPercentage() {
		return destructionPercentage;
	}

	public void setDestructionPercentage(int destructionPercentage) {
		this.destructionPercentage = destructionPercentage;
	}
}
