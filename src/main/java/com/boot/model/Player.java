package com.boot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "player")
public class Player {

	@Id
	@Column(name = "playerid")
	private String playerID;

	@Column(name = "clanid")
	private String clanID;
	
	public Player() {
		
	}

	public Player(String playerID, String clanID) {
		this.playerID = playerID;
		this.clanID = clanID;
	}
	
	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getClanID() {
		return clanID;
	}

	public void setClanID(String clanID) {
		this.clanID = clanID;
	}

}
