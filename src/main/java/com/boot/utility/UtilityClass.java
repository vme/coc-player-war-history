package com.boot.utility;

import java.sql.Timestamp;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public final class UtilityClass {

	private static String apiKey;
	private static Logger logger = LoggerFactory.getLogger("static utility class");

	@Value("${secrets.apiKey}")
	public void setApiKey(String key) {
		apiKey = key;
	}

	/**
	 * Generated the Header entity for the COC API call
	 * 
	 * @return
	 */
	public static HttpEntity<String> generateCOCHeaderEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("authorization", apiKey);
		headers.set("Accept", "application/json");
		HttpEntity<String> entity = new HttpEntity<>(headers);
		logger.info(entity.toString());
		return entity;
	}

	/**
	 * Returns the timestamp for 48 hrs ahead from current time
	 * 
	 * @return Timestamp
	 */
	public static Timestamp getCurrentDateTimePlus48hrs() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 2);
		return new Timestamp(cal.getTime().getTime());
	}

	/**
	 * Return current time timestamp
	 * 
	 * @return Timestamp
	 */
	public static Timestamp getCurrentDateTime() {
		Calendar cal = Calendar.getInstance();
		return new Timestamp(cal.getTime().getTime());
	}

	/**
	 * Method to remove #, if present in the tag
	 * 
	 * @param tag
	 * @return String
	 */
	public static String removeHashInTag(String tag) {
		return tag.replace("#", "");
	}
}
